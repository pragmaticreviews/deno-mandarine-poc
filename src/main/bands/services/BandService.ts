import { Service } from "https://deno.land/x/mandarinets/mod.ts";
import { Band } from "../types/types.ts";

@Service()
export class BandService {

    private bands: Band[] = [
        {
          id: "1",
          name: "Metallica",
          genre: "Metal",
          website: "https://www.metallica.com"
        },
        {
          id: "2",
          name: "Ramones",
          genre: "Punk",
          website: "https://www.ramones.com"
        },
        {
          id: "3",
          name: "Marron 5",
          genre: "pop",
          website: "https://www.maroon5.com"
        },
    ];

    public getBands(): Band[] {
        return this.bands;
    }

}
