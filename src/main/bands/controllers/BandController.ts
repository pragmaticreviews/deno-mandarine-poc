import { Controller, GET } from "https://deno.land/x/mandarinets/mod.ts";
import { BandService } from "../services/BandService.ts";

@Controller()
export class BandController {

    constructor(private readonly bandService: BandService){}

    @GET('/bands')
    public bands() {
        return this.bandService.getBands();
    }
}
