export interface Band {
    id: String,
    name: string,
    genre: string,
    website: string,
}
