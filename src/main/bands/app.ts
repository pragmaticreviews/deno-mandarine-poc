import { Controller, Service, GET, Session, Value, MandarineCore } from "https://deno.land/x/mandarinets/mod.ts";
import { BandController } from "./controllers/BandController.ts";
import { BandService } from "./services/BandService.ts";

const controllers = [BandController];
const services = [BandService];
const middleware = [];
const repositories = [];
const configurations = [];
const components = [];
const otherModules = [];

@Service()
export class MyService {

    @Value("message")
    private message: string;

    public sayHi(): string {
        return this.message;
    }

}

@Controller()
export class MyController {

    constructor(private readonly myService: MyService){}

    @GET('/hello-world')
    public helloWorld(@Session() session: any) {
        if(session.times == undefined) session.times = 0;
        session.times++;

        return `Hello World number #${session.times} (Yes, there are multiple universes)`;
    }

    @GET('/say-hi')
    public sayHi() {
        return `<b>${this.myService.sayHi()}</b>`;
    }
}

new MandarineCore().MVC().run();
